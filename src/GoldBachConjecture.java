import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

/**
 * Created by ANELE on 2015-10-06.
 */
public class GoldBachConjecture {
    public static void main(String[] args)
    {
        new GoldBachConjecture();
    }
    public GoldBachConjecture()
    {
        Scanner scanner = new Scanner(System.in);
        printInstruction();
        while (scanner.hasNextInt())
        {
            test(scanner.nextInt());
            printInstruction();
        }
    }
    private void printInstruction()
    {
        System.out.println("Please enter an integer greater than 2:");
    }

    private void test(int integer)
    {
        if (integer > 2)
        {
            Integer[] factors = getPrimesLessThan(integer);
            boolean satisfied = false;
            int a = 0, b = 0;
            for (int i = 0; i < factors.length && !satisfied; i++)
            {
                a = factors[i];
                for (int k = 0; k < factors.length && !satisfied; k++)
                {
                    b = factors[k];
                    if (a + b == integer)
                    {
                        satisfied = true;
                    }
                }
            }
            if (satisfied)
            {
                System.out.println(integer + "\t = \t"  + a + " + " + b);
            } else

            {
                System.out.println("There were no two primes found that sum to " + integer);
            }
        }
    }

    private Integer[] getPrimesLessThan(int integer)
    {
        List<Integer> ints = new ArrayList<Integer>();
        for (int i = 2; i <= integer; i++)
        {
            if (isPrime(i))
            {
                ints.add(i);
            }
        }

        Integer[] intsArray = new Integer[ints.size()];
        return ints.toArray(intsArray);
    }

    private boolean isPrime(int integer)
    {
        for (int i = 2; i < integer; i++)
        {
            if (integer % i == 0)
                return false;
        }
        return true;
    }
}






