import java.io.BufferedReader;
import java.io.*;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.*;

/**
 * Created by ANELE on 2015-09-21.
 */
public class Anagrams {
    public static void main(String[] args) throws IOException {

        URL url = new URL("http://www.puzzlers.org/pub/wordlists/unixdict.txt");
        InputStreamReader streamReader = new InputStreamReader(url.openStream());
        BufferedReader reader = new BufferedReader(streamReader);

        Map<String, Collection<String>> anagrams = new HashMap<String, Collection<String>>();
        String word;
        int count = 0;
        while ((word = reader.readLine()) != null) {
            char[] chars = word.toCharArray();
            Arrays.sort(chars);
            String key = new String(chars);

            if (!anagrams.containsKey(key))
                anagrams.put(key, new ArrayList<String>());
            anagrams.get(key).add(word);
            count = Math.max(count, anagrams.get(key).size());
        }

        for (Collection<String> anagram : anagrams.values())
            if (anagram.size() >= count)
                System.out.println(anagram);
        reader.close();
    }
}
