/**
 * Created by ANELE on 2015-10-07.
 */
public class BruteForceStringSearch {
    private char[] text;
    private char[] pattern;
    private int textLength;
    private int patternLength;

    public void setString(String text, String pat) {
        this.text = text.toCharArray();
        this.pattern = pat.toCharArray();
        this.textLength = text.length();
        this.patternLength = pat.length();
    }

    public int search() {
        for (int i = 0; i < textLength-patternLength; i++) {
            int j = 0;
            while (j < patternLength && text[i+j] == pattern[j]) {
                j++;
            }
            if (j == patternLength)
                return i;
        }
        System.out.println("No match found");
        return 0;
    }

    public static void main(String[] args) {
        BruteForceStringSearch search = new BruteForceStringSearch();
        String text = "My fundamentals of programming excercises";
        String pattern = "programming";
        search.setString(text, pattern);
        int location = search.search();
        System.out.println("The text '" + pattern + "' is first found after the " + location + " location.");
    }
}
