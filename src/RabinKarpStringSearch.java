import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Random;
import java.math.BigInteger;

/**
 * Created by ANELE on 2015-10-07.
 */
public class RabinKarpStringSearch {

    private String pattern;
    private long patternHashValue;
    private int patternLength;
    private long largePrime;
    private int radix;
    private long RM;

    public RabinKarpStringSearch(String text, String pat) {
        this.pattern = pat;
        radix = 256;
        patternLength = pat.length();
        largePrime = longRandomPrime();

        RM = 1;
        for (int i = 1; i <= patternLength - 1; i++)
            RM = (radix * RM) % largePrime;
        patternHashValue = computeHash(pat, patternLength);
        int pos = checkMatch(text);
        if (pos == -1)
            System.out.println("No match found" );
        else
            System.out.println("Pattern found at position : " + pos);
    }

    private long computeHash(String key, int M) {
        long h = 0;
        for (int j = 0; j < M; j++)
            h = (radix * h + key.charAt(j)) % largePrime;
        return h;
    }

    private boolean check(String txt, int i) {
        for (int j = 0; j < patternLength; j++)
            if (pattern.charAt(j) != txt.charAt(i + j))
                return false;
        return true;
    }

    private int checkMatch(String txt) {
        int N = txt.length();
        if (N < patternLength) return N;
        long txtHash = computeHash(txt, patternLength);
        if ((patternHashValue == txtHash) && check(txt, 0))
            return 0;

        for (int i = patternLength; i < N; i++) {
            txtHash = (txtHash + largePrime - RM * txt.charAt(i - patternLength) % largePrime) % largePrime;
            txtHash = (txtHash * radix + txt.charAt(i)) % largePrime;
            int offset = i - patternLength + 1;
            if ((patternHashValue == txtHash) && check(txt, offset))
                return offset;
        }
        return -1;
    }

    private static long longRandomPrime() {
        BigInteger prime = BigInteger.probablePrime(31, new Random());
        return prime.longValue();
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Rabin Karp Algorithm Test\n");
        System.out.println("Enter Text");
        String text = br.readLine();
        System.out.println("Enter Pattern");
        String pattern = br.readLine();
        System.out.println("Results :");
        RabinKarpStringSearch rkStringsearch = new RabinKarpStringSearch(text, pattern);
    }
}


