import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by ANELE on 2015-09-29.
 */
public class PigLatin {

    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws IOException {

        System.out.print("Please enter a word: ");
        String english = getString();

        String latin = pigLatin(english);
        System.out.println(latin);
    }

    private static String pigLatin(String string) {
        String latin = "";
        int i = 0;
        while (i<string.length()) {

            while (i<string.length() && !isLetter(string.charAt(i))) {
                latin = latin + string.charAt(i);
                i++;
            }

            if (i>=string.length()) break;


            int begin = i;
            while (i<string.length() && isLetter(string.charAt(i))) {
                i++;
            }

            int end = i;
            latin = latin + pigWord(string.substring(begin, end));
        }
        return latin;
    }

    private static boolean isLetter(char c) {
        return ( (c >='A' && c <='Z') || (c >='a' && c <='z') );
    }

    private static String pigWord(String word) {
        int split = firstVowel(word);
        return word.substring(split)+"-"+word.substring(0, split)+"ay";
    }

    private static int firstVowel(String word) {
        word = word.toLowerCase();
        for (int i=0; i<word.length(); i++)
            if (word.charAt(i)=='a' || word.charAt(i)=='e' ||
                    word.charAt(i)=='i' || word.charAt(i)=='o' ||
                    word.charAt(i)=='u')
                return i;
        return 0;
    }

    private static String getString() throws IOException {
        return reader.readLine();
    }
}