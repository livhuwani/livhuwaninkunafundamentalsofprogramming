import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by ANELE on 2015-09-29.
 */
public class BullsAndCows {
    public static void main(String[] args){
        Random generate= new Random();
        int target= 0;
        while(hasDuplications(target= (generate.nextInt(9000) + 1000)));
        String targetString = target +"";
        boolean guessed = false;
        Scanner input = new Scanner(System.in);
        int guesses = 0;
        do{
            int bulls = 0;
            int cows = 0;
            System.out.print("Guess a 4-digit number with no duplicate digits: ");
            int guess;
            try{
                guess = input.nextInt();
                if(hasDuplications(guess) || guess < 1000) continue;
            }catch(InputMismatchException e){
                continue;
            }
            guesses++;
            String guessStr = guess + "";
            for(int i= 0;i < 4;i++){
                if(guessStr.charAt(i) == targetString.charAt(i)){
                    bulls++;
                }else if(targetString.contains(guessStr.charAt(i)+"")){
                    cows++;
                }
            }
            if(bulls == 4){
                guessed = true;
            }else{
                System.out.println(cows+" Cows and " + bulls + " Bulls.");
            }
        }while(!guessed);
        System.out.println("You won after "+ guesses +" guesses!");
    }

    public static boolean hasDuplications(int num){
        boolean[] digits = new boolean[10];
        while(num > 0){
            if(digits[num%10]) return true;
            digits[num%10] = true;
            num/= 10;
        }
        return false;

    }
}
