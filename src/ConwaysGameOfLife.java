/**
 * Created by ANELE on 2015-09-29.
 */
public class ConwaysGameOfLife {
    public static void main(String[] args){
        String[] lifeFields= {"_*_", "_*_", "_*_",};
        int generations= 3;
        for(int i= 0;i < generations;i++){
            System.out.println("Generation " + i + ":");
            print(lifeFields);
            lifeFields= life(lifeFields);
        }
    }

    public static String[] life(String[] lifeFields){
        String[] newGeneration= new String[lifeFields.length];
        for(int row= 0;row < lifeFields.length;row++){
            newGeneration[row]= "";
            for(int i= 0;i < lifeFields[row].length();i++){
                String above= "";
                String same= "";
                String below= "";
                if(i == 0){
                    above = (row == 0) ? null : lifeFields[row - 1].substring(i, i + 2);
                    same= lifeFields[row].substring(i + 1, i + 2);
                    below= (row == lifeFields.length - 1) ? null : lifeFields[row + 1].substring(i, i + 2);

                }else if(i == lifeFields[row].length() - 1){
                    above= (row == 0) ? null : lifeFields[row - 1].substring(i - 1, i + 1);
                    same= lifeFields[row].substring(i - 1, i);
                    below= (row == lifeFields.length - 1) ? null : lifeFields[row + 1].substring(i - 1, i + 1);

                }else{
                    above= (row == 0) ? null : lifeFields[row - 1].substring(i - 1, i + 2);
                    same= lifeFields[row].substring(i - 1, i) + lifeFields[row].substring(i + 1, i + 2);
                    below= (row == lifeFields.length - 1) ? null : lifeFields[row + 1].substring(i - 1, i + 2);
                }

                int neighbors= getNeighbors(above, same, below);
                if(neighbors < 2 || neighbors > 3){
                    newGeneration[row]+= "_";
                }else if(neighbors == 3){
                    newGeneration[row]+= "*";
                }else{
                    newGeneration[row]+= lifeFields[row].charAt(i);
                }
            }
        }
        return newGeneration;
    }

    public static int getNeighbors(String above, String same, String below){
        int answer= 0;
        if(above != null){
            for(char x: above.toCharArray()){
                if(x == '*') answer++;
            }
        }
        for(char x: same.toCharArray()){
            if(x == '*') answer++;
        }
        if(below != null){
            for(char x: below.toCharArray()){
                if(x == '*') answer++;
            }
        }
        return answer;
    }

    public static void print(String[] dish){
        for(String s: dish){
            System.out.println(s);
        }
    }
}


