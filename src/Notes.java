import java.io.*;
import java.nio.channels.*;
import java.text.*;
import java.util.*;

/**
 * Created by ANELE on 2015-10-08.
 */
public class Notes {
    public static void main(String[] args) throws IOException {

        Date date = new Date();
        File file = new File("notes.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        if (args.length > 0) {
            BufferedWriter bw = new BufferedWriter(new FileWriter(file, true));
            bw.write(String.valueOf(date));

            bw.write("\t" + args[0]);
            for (int i = 1; i < args.length; i++)
                bw.write(" " + args[i]);

            bw.newLine();
            bw.close();
        } else {
            FileChannel fc = new FileInputStream(file).getChannel();
            fc.transferTo(0, fc.size(), Channels.newChannel(System.out));
            fc.close();
        }
    }

}
