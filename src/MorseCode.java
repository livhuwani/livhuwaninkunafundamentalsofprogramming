import com.sun.corba.se.spi.orbutil.fsm.Input;

import java.util.*;
import java.util.HashMap;

/**
 * Created by ANELE on 2015-10-08.
 */
public class MorseCode {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Please enter a String for translation:");
        String input = scan.nextLine();
        System.out.println( convertToMorse( input));
        System.out.println( convertToEnglish( input));
    }

    public static String convertToMorse(String s) {
        String answer = "";
        s = s.toLowerCase();
        for (int i=0; i<s.length(); i++) {
            char ch = s.charAt(i);
            if (ch=='a') answer += ".-";
            else if (ch=='b') answer += "-...";
            else if (ch=='c') answer += "-.-.";
            else if (ch=='d') answer += "-..";
            else if (ch=='e') answer += ".";
            else if (ch=='f') answer += "..-.";
            else if (ch=='g') answer += "--.";
            else if (ch=='h') answer += "....";
            else if (ch=='i') answer += "..";
            else if (ch=='j') answer += ".---";
            else if (ch=='k') answer += "-.-";
            else if (ch=='l') answer += ".-..";
            else if (ch=='m') answer += "--";
            else if (ch=='n') answer += "-.";
            else if (ch=='o') answer += "---";
            else if (ch=='p') answer += ".--.";
            else if (ch=='q') answer += "--.-";
            else if (ch=='r') answer += ".-.";
            else if (ch=='s') answer += "...";
            else if (ch=='t') answer += "-";
            else if (ch=='u') answer += "..-";
            else if (ch=='v') answer += "...-";
            else if (ch=='w') answer += ".--";
            else if (ch=='x') answer += "-..-";
            else if (ch=='y') answer += "-.--";
            else if (ch=='z') answer += "--..";
            else if (ch=='0') answer +=  "-----";
            else if (ch=='1') answer += ".----";
            else if (ch=='2') answer +=  "..---";
            else if (ch=='3') answer += "...--";
            else if (ch=='4') answer += "....-";
            else if (ch=='5') answer +=  ".....";
            else if (ch=='6') answer += "-.....";
            else if (ch=='7') answer += "--...";
            else if (ch=='8') answer +=  "---..";
            else if (ch=='9') answer += "---- .";
            else if (ch==' ') answer += "";

            else System.out.println("Your input is invalid");
            answer += " ";
        }
        return answer;
    }

    public static String convertToEnglish(String s) {
        String answer = "";
        s = s.toLowerCase();
        for (int i=0; i<s.length(); i++) {
            String str = " ";
            if (str ==".-") answer += 'a';
            else if (str=="-...") answer += 'b';
            else if (str=="-.-.") answer += 'c' ;
            else if (str== "-..") answer += 'd';
            else if (str==".") answer += 'e';
            else if (str== "..-.") answer += 'f';
            else if (str=="--.") answer += 'g';
            else if (str=="....") answer += 'h';
            else if (str== "..") answer += 'i';
            else if (str==".---") answer += 'j';
            else if (str=="-.-") answer += 'k';
            else if (str==".-..") answer += 'l';
            else if (str=="--") answer += 'm';
            else if (str=="-.") answer += 'n';
            else if (str=="---") answer +='o' ;
            else if (str==".--.") answer += 'p';
            else if (str=="--.-") answer += 'q';
            else if (str==".-.") answer += 'r';
            else if (str=="...") answer += 's';
            else if (str=="-") answer += 't';
            else if (str== "..-") answer += 'u';
            else if (str==".--") answer += 'w';
            else if (str=="-..-") answer += 'x';
            else if (str=="-.--") answer += 'y';
            else if (str=="--..") answer += 'z';
            else if (str=="-----") answer +=  '0';
            else if (str==".----") answer += '1';
            else if (str=="..---") answer +=  '2';
            else if (str== "...--") answer += '3';
            else if (str=="....-") answer += '4';
            else if (str==".....") answer +=  '5';
            else if (str=="-.....") answer += '6';
            else if (str=="--...") answer += '7';
            else if (str== "---..") answer += '8';
            else if (str=="---- .") answer += '9';
            else if (str== " ") answer += ' ';

            else System.out.println("Your input is invalid");
            answer += " ";
        }
        return answer;
    }

}


