
import java.util.BitSet;
import java.util.Scanner;

/**
 * Created by ANELE on 2015-10-07.
 */
public class SieveOfEratosthenes {

    public void primes(int n) {
        int maxIntRoot = (int) Math.sqrt(n);
        int count = 0;
        boolean[] primes = new boolean[n + 1];
        for (int i = 2; i < primes.length; i++) {
            primes[i] = true;
        }
        int num = 2;
        while (true) {
            for (int i = 2;; i++) {
                int multiple = num * i;
                if (multiple > n) {
                    break;
                } else {
                    primes[multiple] = false;
                }
            }
            boolean nextNumFound = false;
            for (int i = num + 1; i < n + 1; i++) {
                if (primes[i]) {
                    num = i;
                    nextNumFound = true;
                    break;
                }
            }
            if (!nextNumFound) {
                break;
            }
        }
        for (int i = 0; i < primes.length; i++) {
            if (primes[i]) {
                System.out.println(i);
            }
        }

        for(int i = 2; i < maxIntRoot; i++){
            if(!primes[i]){
                count++;
                for(int j = i * i; j <= n; j += i)
                    primes[j] = true;
            }
        }
        for(int i = maxIntRoot; i <= n; i++)
            if(!primes[i]){
                count++;
            }
        System.out.println("Number of primes returned" + n + "are" + count);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a number: ");
        int n = scanner.nextInt();
        SieveOfEratosthenes sieve = new SieveOfEratosthenes();
        sieve.primes(n);

    }

}




