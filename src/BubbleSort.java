/**
 * Created by ANELE on 2015-09-21.
 */

import java.util.Scanner;

public class BubbleSort {
    public static void main(String[] args) {

        int numbersToSort[] = new int[]{5, 90, 35, 45, 150, 3};
        System.out.println("Array Before Bubble Sort");
        printArray(numbersToSort);
        bubbleSort(numbersToSort);
        System.out.println("");
        System.out.println("Array After Bubble Sort");
        printArray(numbersToSort);
    }

    private static int printArray(int[] intArray) {
        for (int i = 0; i < intArray.length; i++) {
            System.out.print(intArray[i] + " ");
        }return (0);
    }

    private static int bubbleSort(int[] intArray) {
        int n = intArray.length;
        int temp = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (intArray[j - 1] > intArray[j]) {
                    temp = intArray[j - 1];
                    intArray[j - 1] = intArray[j];
                    intArray[j] = temp;
                }

            }
        } return (n);
    }
}

