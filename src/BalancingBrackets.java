import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * Created by ANELE on 2015-09-23.
 */
public class BalancingBrackets {

    public static boolean checkBrackets(String str) {
        int isBalanced = 0;
        for (char bracket : str.toCharArray()) {
            if (bracket == '[') {
                isBalanced++;
            } else if (bracket == ']') {
                isBalanced--;
            } else {
                return false;
            }
            if (isBalanced < 0) {
                return false;
            }
        }
        return isBalanced == 0;
    }

    public static String generate(int n) {
        if (n % 2 == 1) {
            return null;
        }
        String ans = "";
        int openBracketsLeft = n / 2;
        int unclosed = 0;
        while (ans.length() < n) {
                if (Math.random() >= .5 && openBracketsLeft > 0 || unclosed == 0) {
                    ans += '[';
                openBracketsLeft--;
                unclosed++;
            } else {
                ans += ']';
                unclosed--;
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        String output = "", output1 = "";
        String balanced = "";
        String notBalanced = "";
        String[] tests = {"", "[]", "][", "[][]", "][][", "[[][]]", "[]][[]"};
        for (int i = 0; i < 16; i += 2) {
            generate(i);

        }
        for (String test : tests) {
            if (checkBrackets(test) == true) {
                output = "OK";
                if(test.isEmpty()){
                    test = "(Empty):";
                }
                balanced = test;

            }else{
                output1 = "NOT OK";
                notBalanced = test;
                System.out.printf("%-10s %-10s %-10s %-10s\n", balanced, output, notBalanced, output1);
            }
        }
    }
}

