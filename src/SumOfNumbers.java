/**
 * Created by ANELE on 2015-09-29.
 */
public class SumOfNumbers {
    public static void main(final String[] args) {
        int sum = 0;
        int[] numbers = {1, 2, 3, 4, 5};
        for (int i : numbers) {
            sum += i;
        }
        System.out.println("the sum of all numbers is:" + sum);
    }
}



