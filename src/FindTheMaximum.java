import java.util.Random;
import java.util.Scanner;

/**
 * Created by ANELE on 2015-09-22.
 */
public class FindTheMaximum {

    public static void main(String[] args){

        int[] numbers = {2,25,55,29,35,115};
        int largestValue = Integer.MIN_VALUE;

        for(int i = 0; i < numbers.length; i++) {
            if(numbers[i] > largestValue) {
                largestValue = numbers[i];
            }
        }
        System.out.println("Largest number in array is : " +largestValue);
    }
}



