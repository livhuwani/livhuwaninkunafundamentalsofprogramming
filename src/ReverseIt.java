/**
 * Created by ANELE on 2015-09-29.
 */
public class ReverseIt {

    static int i;

    static void stringreverse(String string) {
        char ch[] = new char[string.length()];
        for (i = 0; i < string.length(); i++)
            ch[i] = string.charAt(i);
        for (i = string.length() - 1; i >= 0; i--)
            System.out.print(ch[i]);
    }

    public static void reverseArray() {
        int arrayToReverse[] = new int[]{10, 20, 30, 50, 70};
        System.out.println("\n"+"Reversing an array:");
        for (int i = 0; i < arrayToReverse.length / 2; i++) {
            int temp = arrayToReverse[i];
            arrayToReverse[i] = arrayToReverse[arrayToReverse.length - i - 1];
            arrayToReverse[arrayToReverse.length - i - 1] = temp;
        }
        for (int i = 0; i < arrayToReverse.length; i++) {
            System.out.print(arrayToReverse[i] + "\t");
        }
    }

    public static void main(String args[]) {
        System.out.println("Original String is : ");
        System.out.println("My fundamentals of programming exercise ");
        ReverseIt.stringreverse("My fundamentals of programming exercise");
        ReverseIt.reverseArray();


    }
}



