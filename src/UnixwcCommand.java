import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by ANELE on 2015-10-06.
 */
public class UnixwcCommand {
    public static void main(String[] args) {
        BufferedReader in = null;
        String fileName = "C:\\Users\\ANELE\\Desktop\\unixwccommand.txt";
        try {
            FileReader fileReader = new FileReader(fileName);
            in = new BufferedReader(fileReader);
            count(fileName, in);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }
    }
    public static void count(String fileName, BufferedReader in){
        try{
            String line;
            String[] words;
            int totalwords = 0;
            long totalwords1 = 0;
            int lines = 0;
            int chars = 0;
            while( (line = in.readLine())!= null){
                lines++;
                chars += line.length();
                words = line.split(" ");
                totalwords += words.length;
                totalwords1 += countWords(line);
            }
            System.out.println("Total Lines :"+lines);
            System.out.println("Total Characters :"+chars);
            System.out.println("Total words :"+totalwords1);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    private static long countWords(String line) {
        long numWords = 0;
        int index = 0;
        boolean prevWhitespace = true;
        while (index < line.length()){
            char c = line.charAt(index++);
            boolean currWhitespace = Character.isWhitespace(c);
            if (prevWhitespace && !currWhitespace) {
                numWords++;
            }
            prevWhitespace = currWhitespace;
        }
        return numWords;
    }
}


