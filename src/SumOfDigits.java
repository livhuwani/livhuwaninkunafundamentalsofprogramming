/**
 * Created by ANELE on 2015-09-29.
 */

import java.util.Scanner;

public class SumOfDigits {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter a number to calculate sum of digits");
        int number = sc.nextInt();

        int sum = 0;
        int input = number;
        while (input != 0) {
            int digit = input % 10;
            sum += digit;
            input /= 10;
        }
        System.out.printf("Sum of digits of number %d is %d", number, sum);

        sc.close();
    }
}







