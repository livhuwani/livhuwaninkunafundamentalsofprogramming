import java.io.File;

/**
 * Created by ANELE on 2015-09-29.
 */
public class WalkTheDirectoryTree {
    public static void main(String[] args) {
        walkin(new File("C:\\Users\\ANELE\\Documents"));
    }

    public static void walkin(File dir) {
        String pattern = ".docx";

        File listFile[] = dir.listFiles();
        if (listFile != null) {
            for (int i=0; i<listFile.length; i++) {
                if (listFile[i].isDirectory()) {
                    walkin(listFile[i]);
                } else {
                    if (listFile[i].getName().endsWith(pattern)) {
                        System.out.println(listFile[i].getPath());
                    }
                }
            }
        }
    }
}


