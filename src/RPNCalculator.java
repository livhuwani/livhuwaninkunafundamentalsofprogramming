import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;

/**
 * Created by ANELE on 2015-10-06.
 */
public class RPNCalculator {

    public static void main(String[] args) {
        LinkedList operands = new LinkedList();
        String operators = "+-*/";
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String inCalcs = reader.readLine();
            String[] inTokens = inCalcs.split(" ");
            for (String string : inTokens) {

                try {
                    Double operand = Double.parseDouble(string);
                    operands.push(operand);
                } catch (NumberFormatException e) {
                    if (string.length() == 1 && operators.contains(string)) {
                        Double op2 = (Double) operands.pop();
                        Double op1 = (Double) operands.pop();
                        switch (string) {
                            case "+":
                                operands.push(op1 + op2);
                                break;
                            case "-":
                                operands.push(op1 - op2);
                                break;
                            case "*":
                                operands.push(op1 * op2);
                                break;
                            case "/":
                                operands.push(op1 / op2);
                                break;
                            default:
                                break;
                        }
                        System.out.format("%s %s %s operandQueue: %s\r\n", op1,
                                op2, string, operands);
                    }
                } catch (NullPointerException e) {
                }
            }
        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }

}
