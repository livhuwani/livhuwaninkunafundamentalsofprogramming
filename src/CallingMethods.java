import java.util.List;
/**
 * Created by ANELE on 2015-09-21.
 */
public class CallingMethods {

    public static int methodCalling() {
        int sum = 1 + 1;
        return sum;

    }

    public static int methodCalling(int number) {
        int sum = number + 2;
        return sum;
    }

    public static void methodCallng(String string, int add) {
        System.out.println(string + add);
    }

    public static double methodCalling(double... values) {
        double largest = Integer.MIN_VALUE;
        for (double v : values)
            if (v > largest) largest = v;
        return largest;
    }

    public static int methodCalling(int n1, int n2) {
        int min;
        if (n1 > n2)
            min = n2;
        else
            min = n1;
        return min;
    }

    public static double methodCalling(double loanAmount, double rate, double futureValue, int years ) {
        double interest = rate / 100;
        double partial = Math.pow((1 + interest), -years);
        double denominator = (1 - partial) / interest;
        double answer = (-loanAmount / denominator) - ((futureValue * partial) / denominator);
        return answer;
    }

    private String privateMethod(String message) {
        System.out.println("This is a private method");
        return message;
    }

    protected void protectedMethod()
    {
        System.out.println("This is a protected method");
    }

    public void publicMethod() {
        System.out.println("This is a public method");
    }

    public static void main(String[] args) {
        CallingMethods call = new CallingMethods();

        int add = call.methodCalling();
        System.out.println("Method without argument is:" + add);

        int add2 = call.methodCalling(30);
        System.out.println("Method with argument is:" + add2);

        call.methodCallng("Method with optional arguments \t", add2);

        double mc  = call.methodCalling(3.1, 5.5, -9);
        System.out.println("Method with a variable number of arguments \t" + mc);

        int min = call.methodCalling(55, 60);
        System.out.println("Method with a return value \t" + min);

        double payment = call.methodCalling(1000.00, 6, 10000.00, 3);
        System.out.println("Method passed by value \t" + payment);

        call.privateMethod(" ");
        call.protectedMethod();
        call.publicMethod();




    }

}
