/**
 * Created by ANELE on 2015-09-29.
 */
public class CommasAndAnds {

    public static String commasAnds(String[] words) {

    String brace = "{";
    for(int inputWords = 0; inputWords < words.length; inputWords++) {
        brace += words[inputWords] + (inputWords == words.length-1 ? "" :
                inputWords == words.length-2 ? " and " : ", ");
    }
        brace += "}";
    return brace;
}

    public static void main(String[] args) {
        System.out.println(commasAnds(new String[]{}));
        System.out.println(commasAnds(new String[]{"ABC"}));
        System.out.println(commasAnds(new String[]{"ABC", "DEF"}));
        System.out.println(commasAnds(new String[]{"ABC", "DEF", "G"}));
        System.out.println(commasAnds(new String[]{"ABC", "DEF", "G", "H"}));

    }
}
